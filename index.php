<?php
    require_once 'animal.php';
    require_once 'ape.php';
    require_once 'frog.php';

    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    $kodok->jump(); // "hop hop"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>